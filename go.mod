module bitbucket.org/josemoreno_boxever/optima-cli

go 1.13

require (
	github.com/aws/aws-sdk-go v1.25.45
	github.com/logrusorgru/aurora v0.0.0-20191116043053-66b7ad493a23
)
