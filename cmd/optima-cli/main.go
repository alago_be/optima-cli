package main

import (
	"fmt"
	"os"
	"time"

	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima/cmd"
	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima/cmd/version"
)

var (
	buildVersion = "development"
	buildID      = "no-build"
	buildDate    = time.Now().String()
)

const (
	errorParsingArg   = 1
	errorExecutingCmd = 2
)

func main() {
	version.Version = buildVersion
	version.Build = buildID
	version.Date = buildDate

	command, err := cmd.ArgsParser()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(errorParsingArg)
	}
	if err := command.Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(errorExecutingCmd)
	}
}
