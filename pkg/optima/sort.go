package optima

import (
	"sort"

	"github.com/aws/aws-sdk-go/service/sfn"
)

type By func(p1, p2 *sfn.ExecutionListItem) bool

func (by By) Sort(executions []*sfn.ExecutionListItem) {
	ps := &executionSorter{
		executions: executions,
		by:         by, // The Sort method's receiver is the function (closure) that defines the sort order.
	}
	sort.Sort(ps)
}

type executionSorter struct {
	executions []*sfn.ExecutionListItem
	by         func(p1, p2 *sfn.ExecutionListItem) bool // Closure used in the Less method.
}

func (s *executionSorter) Len() int {
	return len(s.executions)
}

// Swap is part of sort.Interface.
func (s *executionSorter) Swap(i, j int) {
	s.executions[i], s.executions[j] = s.executions[j], s.executions[i]
}

// Less is part of sort.Interface. It is implemented by calling the "by" closure in the sorter.
func (s *executionSorter) Less(i, j int) bool {
	return s.by(s.executions[i], s.executions[j])
}
