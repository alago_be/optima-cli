package version

import (
	"fmt"
)

const Name = "version"

var (
	Version, Build, Date string
)

type Command struct {
	all bool
}

func NewCommand(args []string) (*Command, error) {
	all := false
	if len(args) == 2 && args[1] == "all" {
		all = true
	}
	return &Command{all: all}, nil
}

func (cmd *Command) Run() error {
	fmt.Printf("Optima CLI version %s\n", Version)
	if cmd.all {
		fmt.Printf("  Build id:   %s\n  Build date: %s\n", Build, Date)
	}
	return nil
}
