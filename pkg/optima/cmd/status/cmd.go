package status

import (
	"fmt"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"
	. "github.com/logrusorgru/aurora"

	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima"
)

const Name = "status"

type Command struct {
}

func NewCommand(args []string) (*Command, error) {
	// TODO parse args and create StatusCommand
	return &Command{}, nil
}

func (cmd *Command) Run() error {
	showPipelines()
	return nil
}

var (
	sess          = session.Must(session.NewSession())
	stepFunctions = sfn.New(sess, aws.NewConfig().WithRegion("eu-west-1"))
)

func showPipelines() {
	var pipelines []*sfn.ExecutionListItem
	params := &sfn.ListStateMachinesInput{}
	if err := stepFunctions.ListStateMachinesPages(params,
		func(page *sfn.ListStateMachinesOutput, lastPage bool) bool {
			//printMasterStateMachines(page)
			p := listPipelines(page)
			pipelines = append(pipelines, p...)
			return page.NextToken != nil
		}); err != nil {
		fmt.Println("Error getting Optima state machines")
		fmt.Println(err)
		fmt.Println("Are you logged in to AWS with right credentials?")
		os.Exit(1)
	}

	date := func(e1, e2 *sfn.ExecutionListItem) bool {
		return e1.StartDate.Sub(*e2.StartDate) > 0
	}
	optima.By(date).Sort(pipelines)
	printPipelines(pipelines)
}

func listPipelines(page *sfn.ListStateMachinesOutput) []*sfn.ExecutionListItem {
	var pipelines []*sfn.ExecutionListItem
	for _, machine := range page.StateMachines {
		if strings.Contains(*machine.Name, "-master") {
			ListExecutions := &sfn.ListExecutionsInput{StateMachineArn: machine.StateMachineArn}
			executions, err := stepFunctions.ListExecutions(ListExecutions)
			if err == nil && len(executions.Executions) != 0 {
				//execs := executions.Executions
				pipelines = append(pipelines, executions.Executions[0])
			}
		}
	}
	return pipelines
}

const dateFormat = "02/01/2006 15:04"

func printPipelines(pipelines []*sfn.ExecutionListItem) {
	for _, p := range pipelines {
		fmt.Printf("%-30s\t%-10s\t%10s\n", nameFromExecutionARN(*p.ExecutionArn), color(*p.Status)(*p.Status), p.StartDate.Format(dateFormat))
	}
}

func nameFromExecutionARN(arn string) string {
	i1 := strings.Index(arn, ":boxever-optima-") + len(":boxever-optima-")
	i2 := strings.LastIndex(arn, "-data-pipeline-master")
	return arn[i1:i2]
}

func color(s string) func(interface{}) Value {
	switch s {
	case "SUCCEEDED":
		return Green
	case "FAILED":
		return Red
	case "ABORTED":
		return Blue
	default:
		return White
	}
}
