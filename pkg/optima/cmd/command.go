package cmd

import (
	"os"

	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima/cmd/help"
	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima/cmd/pipeline"
	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima/cmd/status"
	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima/cmd/usage"
	"bitbucket.org/josemoreno_boxever/optima-cli/pkg/optima/cmd/version"
)

type Command interface {
	Run() error
}

func ArgsParser() (Command, error) {
	cmd, cmdArgs := theCommandIs(os.Args[1:])
	switch cmd {
	case help.Name:
		return help.NewCommand(cmdArgs)
	case pipeline.Name:
		return pipeline.NewCommand(cmdArgs)
	case status.Name:
		return status.NewCommand(cmdArgs)
	case version.Name:
		return version.NewCommand(cmdArgs)
	case usage.Name:
		return usage.NewCommand(cmdArgs)
	default:
		return usage.NewCommand(cmdArgs)
	}
}

func theCommandIs(args []string) (string, []string) {
	const defaultCommand = usage.Name
	switch len(args) {
	case 0:
		return defaultCommand, []string{}
	case 1:
		return args[0], []string{}
	}
	return args[0], args[1:]
}
