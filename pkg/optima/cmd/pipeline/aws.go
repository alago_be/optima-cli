package pipeline

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/logrusorgru/aurora"
)

var inputFiles = map[string]string{
	// labs
	"paddypower-labs": "optima-data-pipeline-master-gaming-input.json",
	"viva-labs":       "optima-data-pipeline-master-input.json",
	"emirates-labs":   "optima-data-pipeline-master-input.json",
	//explore
	"aib-production":             "optima-data-pipeline-master-input.json",
	"aib-uat-production":         "optima-data-pipeline-master-input.json",
	"emirates-production":        "optima-data-pipeline-master-input.json",
	"ryaniar-production":         "optima-data-pipeline-master-input.json",
	"ryaniar-uat-production":     "optima-data-pipeline-master-input.json",
	"ryaniar-viva-production":    "optima-data-pipeline-master-input.json",
	"ryaniar-volaris-production": "optima-data-pipeline-master-input.json",
}

type pipeline struct {
	customer string
	env      string
	account  string
}

func NewPipeline(customer, env string) *pipeline {
	account := "228626570070"
	if env == "production" {
		account = "507754017716"
	}

	return &pipeline{
		customer: customer,
		env:      env,
		account:  account,
	}
}

var (
	sess = session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
)

func stepFunctions() *sfn.SFN {

	return sfn.New(sess, aws.NewConfig().WithRegion("eu-west-1"))
}

func (p pipeline) Start() error {
	client := lambda.New(sess, &aws.Config{Region: aws.String("eu-west-1")})
	_, err := client.Invoke(&lambda.InvokeInput{
		FunctionName: aws.String(p.FuncName()),
		Payload:      p.Payload()})
	// TODO: get Invoke result, get the payload and print pipeline status result, if
	// no error
	return err
}

func (p pipeline) Status() (err error) {
	stepFunctions := sfn.New(sess, aws.NewConfig().WithRegion("eu-west-1"))

	stateMachine, err := stepFunctions.DescribeStateMachine(&sfn.DescribeStateMachineInput{
		StateMachineArn: aws.String(p.StateMachineArn()),
	})
	if err != nil {
		return
	}

	execs, err := stepFunctions.ListExecutions(&sfn.ListExecutionsInput{StateMachineArn: stateMachine.StateMachineArn})
	if err != nil {
		return
	}

	exec, err := stepFunctions.DescribeExecution(&sfn.DescribeExecutionInput{
		ExecutionArn: execs.Executions[0].ExecutionArn,
	})
	if err != nil {
		return
	}

	const linkTemplate = "https://%s.console.aws.amazon.com/states/home?region=%s#/executions/details/%s"

	fmt.Printf("Status:             %s\n", coloredStatus(*exec.Status))
	fmt.Printf("Start date:         %s\n", aurora.Blue(formatDate(exec.StartDate)))
	fmt.Printf("Stop date:          %s\n", aurora.Blue(formatDate(exec.StopDate)))
	fmt.Printf("Link to console:    %s\n", aurora.Blue(fmt.Sprintf(linkTemplate, "eu-west-1", "eu-west-1", *exec.ExecutionArn)))

	// if pipeline is FAILED or ABORTED get last execution history event, to extract
	// what happens
	if *exec.Status == "FAILED" || *exec.Status == "ABORTED" {
		input := &sfn.GetExecutionHistoryInput{
			ExecutionArn: execs.Executions[0].ExecutionArn,
		}
		if err := stepFunctions.GetExecutionHistoryPages(input,
			func(page *sfn.GetExecutionHistoryOutput, lastPage bool) bool {
				if page.NextToken == nil {
					e := page.Events[len(page.Events)-1]
					fmt.Printf("\nState cause:        %s\n", aurora.BrightRed(executionHistoryEventDetail(e)))
				}
				return page.NextToken != nil
			}); err != nil {
		}
	}
	if exec.Output != nil {
		var outputMsg PipelineSuccessfulOutput
		err := json.Unmarshal([]byte(*exec.Output), &outputMsg)
		if err == nil {
			const lineStatusTemplate = "\t%-20s\t\t%s\n"
			fmt.Println("\nStatus:")
			fmt.Println(aurora.Blue("  Data pipeline:"))
			for _, t := range outputMsg.DataPipelineStatus {
				fmt.Printf(lineStatusTemplate, t.Name, t.Status)
			}
			fmt.Println(aurora.Blue("  Stats pipeline:"))
			for _, t := range outputMsg.StatsPipelineStatus {
				fmt.Printf(lineStatusTemplate, t.Name, t.Status)
			}
			fmt.Println(aurora.Blue("  Views pipeline:"))
			for _, t := range outputMsg.ViewsPipelineStatus {
				fmt.Printf(lineStatusTemplate, t.Name, t.Status)
			}
			fmt.Println(aurora.Blue("  Post deployment pipeline:"))
			for _, t := range outputMsg.PostDeploymentStatus {
				fmt.Printf(lineStatusTemplate, t.Name, t.Status)
			}
		}
	}
	return
}

func (p pipeline) History() (err error) {
	fmt.Printf("%s %s executions:\n", aurora.Blue(p.customer), aurora.Blue(p.env))
	stepFunctions := sfn.New(sess, aws.NewConfig().WithRegion("eu-west-1"))

	stateMachine, err := stepFunctions.DescribeStateMachine(&sfn.DescribeStateMachineInput{
		StateMachineArn: aws.String(p.StateMachineArn()),
	})
	if err != nil {
		return
	}

	execs, err := stepFunctions.ListExecutions(&sfn.ListExecutionsInput{StateMachineArn: stateMachine.StateMachineArn})
	if err != nil {
		return
	}

	for _, execution := range execs.Executions {
		fmt.Printf("\tName: %-40s %-20s %s\n", aurora.Blue(*execution.Name), coloredStatus(*execution.Status), formatDate(execution.StartDate))
	}
	return
}

const dateFormat = "02/01/2006 15:04"

func formatDate(t *time.Time) (s string) {
	if t != nil {
		s = t.Format(dateFormat)
	}
	return
}

func (p pipeline) FQName() string {
	return fmt.Sprintf("%s-%s", p.customer, p.env)
}

func (p pipeline) FuncName() string {
	const funcName = "boxever-optima-%s-cloudwatch-run-optima"
	return fmt.Sprintf(funcName, p.FQName())
}

func (p pipeline) StateMachineArn() string {
	var arnTpl = "arn:aws:states:eu-west-1:%s:stateMachine:boxever-optima-%s-data-pipeline-master"
	return fmt.Sprintf(arnTpl, p.account, p.FQName())
}

func (p pipeline) Payload() []byte {
	const payload = `{ "stateMachineConfigFile": "s3://boxever-optima-%s-eu-west-1/config/aws-step-functions/step-functions-input/%s"}`
	return []byte(fmt.Sprintf(payload, p.FQName(), inputFiles[p.FQName()]))
}

func executionHistoryEventDetail(e *sfn.HistoryEvent) string {
	const defaultDetail = "¯\\_(ツ)_/¯"
	switch *e.Type {
	case "ExecutionFailed":
		if e.ExecutionFailedEventDetails == nil {
			return defaultDetail
		}
		return *e.ExecutionFailedEventDetails.Cause
	case "ExecutionAborted":
		if e.ExecutionAbortedEventDetails.Cause == nil {
			return defaultDetail
		}
		return *e.ExecutionAbortedEventDetails.Cause
	default:
		return defaultDetail
	}
}

type PipelineSuccessfulOutput struct {
	DataPipelineStatus   []TaskStatus `json:"dataPipelineStatus,omitempty"`
	StatsPipelineStatus  []TaskStatus `json:"statsPipelineStatus,omitempty"`
	ViewsPipelineStatus  []TaskStatus `json:"viewsPipelineStatus,omitempty"`
	PostDeploymentStatus []TaskStatus `json:"postDeploymentStatus,omitempty"`
}

type TaskStatus struct {
	Name   string    `json:"taskName,omitempty"`
	Status taskState `json:"status,omitempty"`
}

type taskState int

const (
	skipped taskState = iota + 1
	pending
	success
	failed
	aborted
	ended
	unknown
)

func (s taskState) String() string {
	switch s {
	case skipped:
		return fmt.Sprint(aurora.Gray(12, "SKIPPED"))
	case pending:
		return fmt.Sprint(aurora.Yellow("PENDING"))
	case success:
		return fmt.Sprint(aurora.Green("SUCCESS"))
	case failed:
		return fmt.Sprint(aurora.Red("FAILED"))
	case aborted:
		return fmt.Sprint(aurora.BrightRed("ABORTED"))
	case ended:
		return fmt.Sprint(aurora.BrightGreen("ENDED"))
	default:
		return fmt.Sprint(aurora.Yellow("UNKNOWN"))
	}
}

func (s taskState) MarshalJSON() ([]byte, error) {
	switch s {
	case skipped:
		return []byte("SKIPPED"), nil
	case pending:
		return []byte("PENDING"), nil
	case success:
		return []byte("SUCCESS"), nil
	case failed:
		return []byte("FAILED"), nil
	case aborted:
		return []byte("ABORTED"), nil
	case ended:
		return []byte("ENDED"), nil
	default:
		return []byte("UNKNOWN"), nil
	}
}

func (s *taskState) UnmarshalJSON(b []byte) error {
	switch string(b) {
	case "\"SKIPPED\"":
		*s = skipped
	case "\"PENDING\"":
		*s = pending
	case "\"SUCCESS\"":
		*s = success
	case "\"FAILED\"":
		*s = failed
	case "\"ABORTED\"":
		*s = aborted
	case "\"ENDED\"":
		*s = ended
	default:
		*s = unknown
	}
	return nil
}

func coloredStatus(s string) string {
	var c func(interface{}) aurora.Value
	switch s {
	case "SUCCEEDED":
		c = aurora.Green
	case "FAILED":
		c = aurora.Red
	case "ABORTED":
		c = aurora.Yellow
	default:
		c = aurora.White
	}
	return fmt.Sprint(c(s))
}
