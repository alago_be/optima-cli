#/usr/bin/env bash

# based on https://iridakos.com/programming/2018/03/01/bash-programmable-completion-tutorial


function customers() {
  echo "aib aib-uat emirates paddypower ppb qatar ryanair ryanair-uat spinair volaris"
}

function envs() {
  echo "dev labs production"
}

function _optima_subcommand_param1_completions() {
  case "${COMP_WORDS[1]} ${COMP_WORDS[2]}" in
    "pipeline start")   COMPREPLY=($(compgen -W "$(envs)" "${COMP_WORDS[4]}")) ;;
    "pipeline status")  COMPREPLY=($(compgen -W "$(envs)" "${COMP_WORDS[4]}")) ;;
    "pipeline history") COMPREPLY=($(compgen -W "$(envs)" "${COMP_WORDS[4]}")) ;;
  esac
}

function _optima_subcommand_completions() {
  case "${COMP_WORDS[1]} ${COMP_WORDS[2]}" in
    "pipeline start")   COMPREPLY=($(compgen -W "$(customers)" "${COMP_WORDS[3]}")) ;;
    "pipeline status")  COMPREPLY=($(compgen -W "$(customers)" "${COMP_WORDS[3]}")) ;;
    "pipeline history") COMPREPLY=($(compgen -W "$(customers)" "${COMP_WORDS[3]}")) ;;
  esac
}

function _optima_command_completions() {
  local pipeine_subcommand="start status history"

  case ${COMP_WORDS[1]} in
    pipeline) COMPREPLY=($(compgen -W "$pipeine_subcommand" "${COMP_WORDS[2]}")) ;;
  esac
}

function _optima_completions() {
  local commands="help pipeline status version usage"

  case ${#COMP_WORDS[@]} in
    2) COMPREPLY=($(compgen -W "$commands" "${COMP_WORDS[1]}")) ;;
    3) _optima_command_completions ;;
    4) _optima_subcommand_completions ;;
    5) _optima_subcommand_param1_completions ;;
  esac
}

complete -F _optima_completions optima