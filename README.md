# Optima CLI

**Optima CLI** is a command line tool to operate with Boxever Optima.

You can install from binaries or build from source.

## Install Optima CLI

[Download the archive](https://bitbucket.org/josemoreno_boxever/optima-cli/downloads/) and extracxt into `/usr/local/`. For example:

```bash
tar -C /usr/local -zxf optima-cli0.2.0-RC2.darwin-amd64.tar.gz
```

Then you can create a symbolic link from `/usr/local/optima-cli/bin/optima` to `/usr/local/bin/optima`

```bash
ln -s -f /usr/local/optima-cli/bin/optima /usr/local/bin/optima
```

(Typically these commands must be run as root or through `sudo`).

### Test the installation

Check that Optima CLI is installec correctly by executing the `version` command:

```bash
optima version
```

## Building from source

### Requisites and configuration

- [Go 1.13+](https://golang.org) as programming language and SDK.
- [Make](https://www.gnu.org/software/make/) as build automation tool.
- [Asciidoctor](https://asciidoctor.org/) as documentation generation tool.

#### Configuring development environment

In MacOSX `make` utility is installed with `xcode` so, in general, nothing must be intalled.

To install *Go* you can use `brew install go` or go to [Go download page](https://golang.org/dl/) and follow the instructions for your platform.

To install *Asciidoctor* you can use `brew install asciidoctor` or go to [Asciidoctor installation page](https://asciidoctor.org/#installation) and follow the instructions for your platform.

### Build

```bash
make
```

You can `make install` to copy the previous built program.

```bash
make install
```

## Running Optima CLI

Onece you have installed the program:

```bash
optima <command> <subcommand> [parameters]
```

To see all options run `optima usage` or `optima help`. If you have installed optima from souce files you can access the man page: `man optima`
